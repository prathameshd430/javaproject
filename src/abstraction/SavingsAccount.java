package abstraction;

public class SavingsAccount implements Account{
    private double accountBalance;

    public SavingsAccount(double accountBalance) {
        this.accountBalance = accountBalance;
        System.out.println("Savings account is created");
    }

    @Override
    public void deposit(double amount) {
        if (amount>0) {
            accountBalance += amount;
            System.out.println(amount + " Rs is credited to your account");
        }
    }

    @Override
    public void withdraw(double amount) {
        if(amount<=accountBalance && amount>0) {
            accountBalance -= amount;
            System.out.println(amount + " Rs is debited from your account");
        }else
            System.out.println("Insufficient account balance");
    }

    @Override
    public void checkBalance() {
        System.out.println("Active balance is "+accountBalance);
    }
}
