package abstraction;

//Factory class
public class AccountFactory {

    //Factory method
    Account createAccount(int type, double balance){
        Account a1=null;
        if (type==1){
            a1=new SavingsAccount(balance);
        } else if (type==2) {
            a1=new LoanAccount(balance);
        }else {
            System.out.println("Invalid account type");
        }
        return a1;
    }
}
