package abstraction;
//Step1--> Create an interface
public interface Account {
    void deposit(double amount);
    void withdraw(double amount);
    void checkBalance();
}
