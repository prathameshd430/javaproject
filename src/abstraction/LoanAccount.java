package abstraction;

//Implementation class
public class LoanAccount implements Account{
    private double loanAmount;

    //Implementation methods
    public LoanAccount(double loanAmount) {
        this.loanAmount = loanAmount;
        System.out.println("Loan account created");
    }

    @Override
    public void deposit(double amount) {
        if(amount>0) {
            loanAmount -= amount;
            System.out.println(amount+" Rs debited from your loan account");
        }
    }

    @Override
    public void withdraw(double amount) {
        if (amount>0){
            loanAmount+=amount;
            System.out.println(amount+" Rs credited to your loan account");
        }
    }

    @Override
    public void checkBalance() {
        System.out.println("Active loan balance is "+loanAmount);
    }
}
