package abstraction;

import java.util.Scanner;
//utilization class
public class Main {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Select account type");
        System.out.println("1:Savings\n2:Loan");
        int accType = sc1.nextInt();
        System.out.println("Enter account balance amount");
        double balance = sc1.nextDouble();

        AccountFactory factory = new AccountFactory();
        Account accRef = factory.createAccount(accType, balance);
        boolean status = true;
        while (status){
            System.out.println("Select mode of transaction");
            System.out.println("1:Deposit\n2:Withdraw\n3:Check balance\n4:exit");
            int choice = sc1.nextInt();
            if(choice==1){
                System.out.println("Enter amount");
                double amt = sc1.nextDouble();
                accRef.deposit(amt);
            } else if (choice==2) {
                System.out.println("Enter amount");
                double amt = sc1.nextDouble();
                accRef.withdraw(amt);
            } else if (choice==3) {
                accRef.checkBalance();
            }else{
                status=false;
            }
        }
    }
}
