package array;

public class arrayP11 {
    public static void main(String[] args) {
        int rows=4;
        int cols=4;
        char c='A';
        for(int a=0;a<rows;a++){
            for(int b=0;b<cols;b++){
                System.out.print(c+++" ");
                if (c>'E'){
                    c='A';
                }
            }
            System.out.println();
        }
    }
}
