package array;
/*\
1 2 3 4
2 4 6 8
3 6 9 12
4 8 12 16
 */
public class arrayP7 {
    public static void main(String[] args) {
        int rows=5;
        int cols=5;
        for (int a=0;a<rows;a++){
            int no=1;
            for (int b=0;b<cols;b++){
                System.out.print(no+" ");
                no++;
            }
            System.out.println();
        }
    }
}
