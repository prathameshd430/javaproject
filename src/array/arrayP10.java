package array;
/*
1 2 3 4 5
6 7 1 2 3
4 5 6 7 1
2 3 4 5 6
 */
public class arrayP10 {
    public static void main(String[] args) {
        int rows=4;
        int cols=5;
        int no=1;
        for(int a=0;a<rows;a++){
            for(int b=0;b<cols;b++){
                System.out.print(no+++" ");
                if (no>7){
                    no=1;
                }
            }
            System.out.println();
        }
    }
}
